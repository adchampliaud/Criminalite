from openpyxl import load_workbook
import mysql.connector


# ###  DB ###
ip = "localhost"
user = "hyde"
pwd = "boop"
db = "criminalite"
timeout = 5

try:
    conn = mysql.connector.connect(host=ip, user=user, password=pwd, database=db, connect_timeout=timeout)
    cursor = conn.cursor()
    
    insert_sql = 'INSERT INTO infraction VALUES ((SELECT id_dep FROM departement WHERE num_dep LIKE %s),%s,(SELECT id_periode FROM periode WHERE date_periode = %s),%s)'
    
except Exception as ex:
    print("Connexion impossible Erreur : " + str(ex))

# ### END DB ###

print('Workbook Loading')
workBook = load_workbook('/home/hyde/Bureau/script/python/Criminalite/ressources/Tableaux_4001_TS.xlsx')
print('Workbook Loaded')

# parametres pour parcourir le fichier
start_sheet = 3;
end_sheet = len(workBook.get_sheet_names())
start_row = 1
end_row = None
start_col = 3
end_col = None

# parcourt le fichier

for n in range(start_sheet, end_sheet): #Parcours les feuilles de la 3eme a la derniere

    sheet = workBook.worksheets[n]
    print(sheet.title + "Progression : ")  # affiche sur quelle feuille nous sommes
    #lastCol = sheet.max_column
    
    for col in sheet.iter_cols(min_row=start_row, max_row = end_row, min_col = start_col, max_col = end_col):
        date = None
        for cell in col:
            if cell.row == 1:
                #on sauvegarde la date 
                rawDate = cell.value.split('_')
                date = rawDate[0]+"-"+rawDate[1]+"-01" 
                print(date);
            else:
                #on ajoute a la db
                try:
                    data_for_insert = (sheet.title, cell.row-1, date, cell.value)
                    cursor.execute(insert_sql,  data_for_insert)
                except Exception as ex:
                    print("Erreur execute : " + str(ex))
                
        
    #on commit (inscrit dans la db)par sheet
    try:
        conn.commit()
    except Exception as ex:
        print("Erreur commit : " + str(ex))
try:                    
    cursor.close()
    conn.close()  
except Exception as ex:
    print("Erreur close conn and cursor : " + str(ex))
                
                
