#Routes

- **get_date()**
   - **Use :** Au chargement de la page pour définir les dates min et max possible
   - **Param :** none
   - **Return :** Object{
                        String date_min,
                        String date_max
                        }
      - **Format :** 
         - date_min, date_max : dd-mm-yyyy
    
- **get_departement()**
   - **Use :** Au chargement de la page récupère la liste des départements (si has_data est a false le dep sera grisé dans le comboBox)
   - **Param :** String date_min, String date_max
      - **Format :** 
         - date_min, date_max : dd-mm-yyyy
   - **Return :** Json Array[
                        Object{
                           int index_dep,
                           string label_dep,
                           string num_dep,
                           bool has_data
                              }]
      - **Format :**
         - index_dep : signed
         - label_dep : 
         - num_dep : 
         - has_data : true or false
    
- **get_type_infraction()**
   - **Use :** Récupère la liste des infractions et leurs unités
   - **Param :** none
   - **Return :** Json Array[
                        Object{
                           int index_type_infraction,
                           string label_type_infraction,
                           Object unite {
                                    int index_unite,
                                    string label_unite
                                    }
                             }]
         - **Format :**
            - index_type_infraction : unsigned
            - label_type_infraction :
            - index_unite : unsigned
            - label_unite : 
    
- **get_infraction()**
   - **Use :** Récupère la liste des valeurs pour une ou plusieurs infractions suivant la date min max et le departement
   - **Param :** Array[Object(first only){string date_min, string date_max}, Object(others){int index_dep, int index_type_infraction}]
   - **Return :** Json Array[
                        Object{
                           int index_dep,
                           int index_type_infraction,
                           Object periode{
                                    int index_periode,
                                    string date
                                    },
                           int valeur
                           }]
      - **Format :**
         - index_dep : signed
         - index_type_infraction : unsigned
         - index_periode : unsigned
         - date, date_min, date_max : dd-mm-yyyy (eg : 01-08-2016)
         - valeur : unsigned
