#Crime Machine

##Environnement de travail

###Logiciels & Outils requis

- IDE : Eclipse Neon
- Serveur applicatif : TomCat port 8080
- Serveur BDD : wamp port 9090
- Uml : StarUML (attention pas la version windows 95 degueulasse)

###Technologies

- Organisation du projet : Maven
- Hibernate
- Spring
- GWT

###Divers

- Variables, fonctions et noms de classe en anglais.
- Commentaires en français.
- Mise en forme du code : formatageCode.xml
- 

NB : Fournir fichier de config eclipse pour formatage du texte automatique.
