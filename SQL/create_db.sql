-- ======================== --
-- CREATION BASE DE DONNEES --
-- ======================== --
DROP DATABASE IF EXISTS `criminalite`;
CREATE DATABASE IF NOT EXISTS `criminalite` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `criminalite`;

-- =============== --
-- CREATION TABLES --
-- =============== --

-- PARTIE INFRACTIONS --
CREATE TABLE `unite` 
(
	`id_unite` int(2) UNSIGNED NOT NULL auto_increment primary key,
	`label_unite` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--CREATE TABLE `groupe_type`
--(
--	`ìd_groupe` int(3) UNSIGNED NOT NULL auto_increment primary key,
--	`label_groupe` varchar(250) NOT NULL
--) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `departement` 
(
	`id_dep` int(3) UNSIGNED NOT NULL auto_increment primary key,
	`num_dep` varchar(3) NOT NULL,
	`label_dep` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `periode` 
(
	`id_periode` int(4) UNSIGNED NOT NULL auto_increment primary key,
	`date_periode` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `type_infra` 
(
	`id_type_infra` int(3) UNSIGNED NOT NULL auto_increment primary key,
	`id_unite` int(2) UNSIGNED NOT NULL,
	`label_type_infra` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `infraction` 
(
	`id_dep` int(3) UNSIGNED NOT NULL,
	`id_type_infra` int(3) UNSIGNED NOT NULL,
	`id_periode` int(4) UNSIGNED NOT NULL,
	`valeur` int(6) UNSIGNED NOT NULL,
	primary key (`id_dep`,`id_type_infra`,`id_periode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- PARTIE EVENEMENTS --
CREATE TABLE `type_eve`
(
	`id_type_eve` int(3) UNSIGNED NOT NULL auto_increment primary key,
	`label_type_eve` varchar(200) NOT NULL	
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `evenement`
(
	`id_evenement` int(4) UNSIGNED NOT NULL auto_increment primary key,
	`id_type_eve` int(3) UNSIGNED NOT NULL,
	`label_evenement` varchar(100) NOT NULL,
	`detail` varchar(1000) NOT NULL,
	`date_debut` date NOT NULL,
	`date_fin` date
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- =========== --
-- FOREIGN KEY --
-- =========== --

-- FK INFRACTION --
ALTER TABLE `infraction`
	ADD CONSTRAINT `FK_INFRACTION_DEPARTEMENT` FOREIGN KEY (`id_dep`) 
		REFERENCES `departement` (`id_dep`),
	ADD CONSTRAINT `FK_INFRACTION_PERIODE` FOREIGN KEY (`id_periode`) 
		REFERENCES `periode` (`id_periode`),
	ADD CONSTRAINT `FK_INFRACTION_TYPE` FOREIGN KEY (`id_type_infra`) 
		REFERENCES `type_infra` (`id_type_infra`);

ALTER TABLE `type_infra`
	ADD CONSTRAINT `FK_TYPE_UNITE` FOREIGN KEY (`id_unite`) 
		REFERENCES `unite` (`id_unite`);

-- FK EVENEMENTS --

ALTER TABLE `evenement` 
	ADD CONSTRAINT `FK_EVENEMENT_TYPE` FOREIGN KEY (`id_type_eve`)
		REFERENCES `type_eve` (`id_type_eve`);