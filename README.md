# Criminalite

## Description

Analyse des crimes et délits en france par départements sur une période de 20 ans

Crime Machine est un logiciel en 2 parties :

- Une partie Script en Python pour générer la base de données a partir du fichier excel.
- Une partie Web en PHP et JavaScript pour les utilisateurs, l'affichage des données et l'ajout des événements.

L'idée était de mettre en corrélation des infractions avec des évènements.

**Ce projet a été réalisé en groupe :**

- [Jeremy FERRER](https://github.com/Ferrer-Jeremy).
- [Adrien CHAMPLIAUD](https://github.com/AdrienCHAMPLIAUD).

## Source de données

###Infractions :

- Ces données ont été récupérées depuis le site [data.gouv](https://www.data.gouv.fr/fr/datasets/chiffres-departementaux-mensuels-relatifs-aux-crimes-et-delits-enregistres-par-les-services-de-police-et-de-gendarmerie-depuis-janvier-1996) Version TS fournis par le ministère de l'intérieur.
- Comme précisé dans le document explicatif fournis sur data.gouv, les chiffres ne correspondent pas forcément à la réalité, les chiffres sont comptabilisés à l'endroit et au moment ou l'administration l'a constaté.
- Ne sont pris en compte que les crimes et délits à l'exclusion des infractions routière.
- **Les infractions sont comptabilisées par départements, par mois et par type d'infraction de janvier 1996 à septembre 2016.**

### Evènements :

- Ces données sont actuellement vide.
- A terme, elles devraient pouvoir être de tout type (événements sportif, mandat électoral ...etc).
- **Elles sont triées par type d'événement et ont une date précise ou une période.**

## Version Web

La version Web permet à l'utilisateur d'afficher des données sous formes de courbes par l'intermediaire de la librairie JavaScript Morris.

## Ajouts prévus

- Ajout des événements sur le même graphique.
- Créations d'événements.
- Affichage de plusieurs courbes dans le même graphique.
- Affichage de diverses statistiques sous forme de tableau de bord.
- Lissage des courbes.
- Légende gérée dynamiquement.
- Peaufinage du graphisme du site.
- Nettoyage du code.
- Mise au propre des noms des types d'infraction.
- Importation de seulement une partie du fichier de données(Ex : par département, par date).
- Importation d'une base de données Evénements.
- Peaufinage du graphisme du logiciel.
